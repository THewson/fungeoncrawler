﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewRoom", menuName = "FungeonCrawler/Room")]
public class SOBJ_Room : ScriptableObject {
    public GameObject m_Entry; // The entry point of a room
    public GameObject m_Exit; // The exit point of that same room

    public GameObject m_RoomRoot; // The Room as a whole
}
