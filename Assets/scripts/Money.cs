﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour {

    public int m_value = 1;

    public float m_upwardVelocity = 1;
    public float m_sideVelocity = 1;

    public float m_gravity = 1;

    public bool m_moving = true;

    // Use this for initialization
    void Start () {
        m_upwardVelocity = Random.Range(0, m_upwardVelocity);
        m_sideVelocity = Random.Range(-m_sideVelocity, m_sideVelocity);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position += new Vector3(m_sideVelocity, m_upwardVelocity, 0);
        m_upwardVelocity -= m_gravity;
	}

    void CheckGround() {
        RaycastHit hit;
        // detects if there is a floor underneath the coin
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1f)) {
            if (hit.transform.tag == "Floor") {
                // if the hit is a floor, stops the coin from moving

            }
        }
    }

    private void OnCollisionEnter(Collision collision) {
        // if the coin touches a floor, does a raycast check

    }
}
