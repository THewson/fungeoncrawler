﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
    [Header("Base Index List")]
    public List<SOBJ_Room> m_BaseRoomIndex;
    public List<SOBJ_Room> m_BaseBossRoomIndex;
    public int m_LevelLength;
    
    [Header("Current Game Information")]
    public SOBJ_Room m_CurrentRoom;
    public List<SOBJ_Room> m_CurrentLevel;
    public int m_CurrentLevelInt;
    public List<SOBJ_Room> m_OpenBaseRooms;
    
    private void Start() {
        RefreshRooms();
        GenerateNewLevel();
        TravelToNextRoom();
    }

    // Replenishes the room list with a copy of the base room index
    public void RefreshRooms() {
        m_OpenBaseRooms = new List<SOBJ_Room>(m_BaseRoomIndex);
    }

    // Generate a new game level
    public void GenerateNewLevel() {
        // Removes the old level data
        m_CurrentLevel = new List<SOBJ_Room>();

        // If there aren't enough rooms for another level to be generated the list is refreshed
        if(m_LevelLength < m_OpenBaseRooms.Count) {
            RefreshRooms();
        }

        // Gets a random avaliable room and adds it to the new list, then removes it from the open room list.
        int num = 0;
        for (int i = 0; i!= m_LevelLength; i++) {
            num = Random.Range(0, m_OpenBaseRooms.Count);

            m_CurrentLevel.Add(m_OpenBaseRooms[num]);
            m_OpenBaseRooms.RemoveAt(num);
        }
    }

    public void TravelToNextRoom() {
        // If there isn't a room assigned
        if(m_CurrentRoom == null) {
            m_CurrentLevelInt = 0;
            m_CurrentRoom = m_CurrentLevel[0];
        }
        // If it's the last room in the level
        else if(m_CurrentRoom == m_CurrentLevel[m_CurrentLevel.Count]) {
            GenerateNewLevel();
            m_CurrentRoom = m_CurrentLevel[0];
            m_CurrentLevelInt = 0;
        }
        // Iterate onto next level
        else {
            m_CurrentLevelInt++;
            m_CurrentRoom = m_CurrentLevel[m_CurrentLevelInt];
        }
    }

}
