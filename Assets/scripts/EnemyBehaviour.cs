﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public int m_level = 1;

    public int m_baseHealth = 100;
    public int m_baseDamage = 5;

    public float m_movementSpeed = 1;

    public int m_experience = 10;

    public int m_minMoney = 1;
    public int m_maxMoney = 2;

    public Player m_player;

    public GameObject m_moneyObject;

    //public void ApplyDamage(int flatDamage, List<int> elementList) {
    //    int totalDamage = flatDamage;
    //    if (elementList.Find(enumelement.fire)) {
    //        totalDamage += 1;
    //    }

    //    currenthealth -= totalDamage;
    //}


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Die() {
        int spawnAmount = Random.Range(m_minMoney,m_maxMoney);
        // spawns coins based on the mix/max coin drop value
        for (int i = 0; i < spawnAmount; i++) {
            Instantiate(m_moneyObject);
        }
    }

    // damages by the given amount
    public void TakeDamage(int damage) {

    }

}
